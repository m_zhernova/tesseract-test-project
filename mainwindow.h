#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <leptonica/allheaders.h>
#include <tesseract/baseapi.h>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
   Q_OBJECT

public:
   MainWindow(QWidget *parent = nullptr);
   ~MainWindow();

private slots:
   void on_openButton_clicked();

   void on_getTextButton_clicked();

private:
   Ui::MainWindow *ui;
   QString fileName;
   tesseract::TessBaseAPI* tessApi;
   const char *lang;
};
#endif // MAINWINDOW_H
