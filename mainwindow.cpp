#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>

#include <tesseract/baseapi.h>

MainWindow::MainWindow(QWidget *parent)
   : QMainWindow(parent)
   , ui(new Ui::MainWindow)
{
   ui->setupUi(this);
   ui->comboBox->addItem("eng");
   ui->comboBox->addItem("rus");
   ui->comboBox->addItem("ukr");
   ui->comboBox->setCurrentIndex(0);
   tessApi = new tesseract::TessBaseAPI();
   tessApi->Init( "E:\\source\\repos\\TesseractExample\\tesseract-test-project\\data", "eng");
}

MainWindow::~MainWindow()
{
   delete ui;
}


void MainWindow::on_openButton_clicked()
{
   fileName = QFileDialog::getOpenFileName(this,
          tr("Choose a picture"), "",
          tr("Images (*.png *.tiff *.tif *.jpg)"));
   QPixmap pm(fileName);

   ui->label->setBackgroundRole(QPalette::Dark);
   ui->label->setPixmap(pm);
}

void MainWindow::on_getTextButton_clicked()
{
   if(fileName.isEmpty())
      return;
   QByteArray ba = ui->comboBox->currentText().toLocal8Bit();
   lang = ba.data();
   tessApi->Init( "E:\\source\\repos\\TesseractExample\\tesseract-test-project\\data", lang);
   PIX *pix = pixRead( fileName.toStdString().c_str() );
   tessApi->SetImage( pix );
   QString result = tessApi->GetUTF8Text();
   pixDestroy( &pix );
   tessApi->Clear();
   ui->textEdit->setText(result);
}
