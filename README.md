# Tesseract test project#

Simple application that gives you the ability to use tesseract library for text recognition. 

### Get it working ###

Open the project in QT Creator. Set the correct absolute path to the *data* folder in tessApi->Init function(lines 18 and 44 in mainwindow.cpp). Try to build, then copy files from the *copy to build directory* folder to your build directory. Try to run the application..

### Screenshots ###

![](screenshots/screenshot1.png)
![](screenshots/screenshot2.png)
![](screenshots/screenshot3.png)